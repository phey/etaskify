FROM ictcontact/openjdk:jdk-11
ADD build/libs/etaskify-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]
