INSERT INTO organization (name, phone_number, address)
VALUES ('Test Organization', '+994123456789', 'Test Address');

INSERT INTO public."user" (email, username, name, surname, password, organization_id, role_id)
VALUES ('test@test.com', 'admin', 'Name', 'Surname', '$2a$10$zq9Yk3m4cSDnZOXypyVOXeastZufpgXfuouTdrNDrjf7jOAnCnvg6', 1,
        1);
