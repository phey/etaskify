package com.etaskify.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("Task model")
public class TaskDto {

    @ApiModelProperty("Task id")
    private Long id;

    @ApiModelProperty("Task name")
    private String title;

    @ApiModelProperty("Task description")
    private String description;

    @ApiModelProperty("Task deadline date")
    private LocalDate deadline;

    @ApiModelProperty("Task status")
    private boolean completed;

    @ApiModelProperty("Task assigner id")
    private List<UserDto> users;

    @ApiModelProperty("Organization data")
    private OrganizationDto organization;
}
