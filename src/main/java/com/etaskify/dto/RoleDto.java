package com.etaskify.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("User role model")
public class RoleDto {

    @ApiModelProperty("Role id")
    private Long id;

    @ApiModelProperty("Role name")
    private String name;
}


