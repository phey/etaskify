package com.etaskify.dto;

public abstract class Constants {
    public static final String EXISTS_BY_USERNAME = "Username is already in use";
    public static final String EXISTS_BY_EMAIL = "Email is already in use";
    public static final String ORGANIZATION_NOT_FOUND = "Organization not found";
    public static final String TASK_NOT_FOUND = "Task not found";
    public static final String ROLE_NOT_FOUND = "Role not found";
    public static final String SUCCESSFULLY_REGISTERED = "User successfully registered";
    public static final String USERS_NOT_FOUND = "Users not found";
    public static final String USER_NOT_FOUND = "User not found";
    public static final String DEFAULT_PASSWORD = "defaultPassword";
}
