package com.etaskify.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("User dto model")
public class UserDto {

    @ApiModelProperty("User id")
    private Long id;

    @ApiModelProperty("User email address")
    private String email;

    @ApiModelProperty("User name")
    private String name;

    @ApiModelProperty("User surname")
    private String surname;

    @ApiModelProperty("Username for login")
    private String username;

    @ApiModelProperty("Organization data")
    private OrganizationDto organization;

    private RoleDto role;
}
