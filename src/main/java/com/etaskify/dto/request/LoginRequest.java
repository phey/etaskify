package com.etaskify.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("Login request model")
public class LoginRequest {

    @ApiModelProperty("Username for login")
    private String username;

    @ApiModelProperty("Password for login")
    private String password;

}
