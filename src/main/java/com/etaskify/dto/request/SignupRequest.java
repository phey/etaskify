package com.etaskify.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("Sign up request model")
public class SignupRequest {

    @ApiModelProperty("Username for login")
    private String username;

    @ApiModelProperty("User name")
    private String name;

    @ApiModelProperty("User surname")
    private String surname;

    @ApiModelProperty("User email")
    @Email
    private String email;

    @ApiModelProperty("User password")
    @Size(min = 6)
    private String password;

    @ApiModelProperty("Organization name")
    private String organizationName;

    @ApiModelProperty("Organization phone number")
    private String organizationPhoneNumber;

    @ApiModelProperty("Organization address")
    private String organizationAddress;
}
