package com.etaskify.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("Create user request model")
public class CreateUserRequest {

    @ApiModelProperty("User email address")
    private String email;

    @ApiModelProperty("User name")
    private String name;

    @ApiModelProperty("User surname")
    private String surname;

    @ApiModelProperty("Username for login")
    private String username;
}
