package com.etaskify.dto;

import com.etaskify.dao.TaskEntity;
import com.etaskify.dao.UserEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("Organization model")
public class OrganizationDto {

    @ApiModelProperty("Organization id")
    private Long id;

    @ApiModelProperty("Organization name")
    private String name;

    @ApiModelProperty("Organization phone number")
    private String phoneNumber;

    @ApiModelProperty("Organization address")
    private String address;

    @ApiModelProperty("Organization users")
    @JsonIgnore
    private Set<UserEntity> users;

    @ApiModelProperty("Organization tasks")
    @JsonIgnore
    private Set<TaskEntity> tasks;
}


