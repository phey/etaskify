package com.etaskify.service;

import com.etaskify.dto.UserDto;
import com.etaskify.dto.request.CreateUserRequest;
import com.etaskify.dto.response.MessageResponse;

import java.util.List;

public interface UserService {

    MessageResponse createUser(CreateUserRequest request, String organizationId);

    List<UserDto> getUsersByOrganization(String organizationId);

    List<UserDto> findAll();

    UserDto getUserById(Long userId);

    UserDto getUserByIdAndOrganization(Long userId, String organizationId);

    void deleteUser(Long id);

    void deleteUserByOrganization(Long id, String organizationId);
}
