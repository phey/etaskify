package com.etaskify.service;

import com.etaskify.dto.request.LoginRequest;
import com.etaskify.dto.request.SignupRequest;
import com.etaskify.dto.response.JwtResponse;
import com.etaskify.dto.response.MessageResponse;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

public interface AuthService {

    JwtResponse authenticateUser(LoginRequest loginRequest, HttpServletResponse response);

    MessageResponse registerUser(@Valid @RequestBody SignupRequest signUpRequest);
}
