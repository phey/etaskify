package com.etaskify.service;

import java.io.IOException;

public interface MailSenderService {
    void sendMail(String emailAddress) throws IOException;
}
