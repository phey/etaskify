package com.etaskify.service.impl;

import com.etaskify.dao.OrganizationRepository;
import com.etaskify.dao.TaskEntity;
import com.etaskify.dao.TaskRepository;
import com.etaskify.dto.TaskDto;
import com.etaskify.mapper.OrganizationMapper;
import com.etaskify.mapper.TaskMapper;
import com.etaskify.mapper.UserMapper;
import com.etaskify.service.MailSenderService;
import com.etaskify.service.TaskService;
import com.etaskify.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.etaskify.dto.Constants.ORGANIZATION_NOT_FOUND;
import static com.etaskify.dto.Constants.TASK_NOT_FOUND;

@Service
public class TaskServiceImpl implements TaskService {
    private final Logger logger = LoggerFactory.getLogger(TaskServiceImpl.class);
    private final TaskRepository taskRepository;
    private final OrganizationRepository organizationRepository;
    private final UserService userService;
    private final MailSenderService mailSenderService;

    public TaskServiceImpl(TaskRepository taskRepository,
                           OrganizationRepository organizationRepository,
                           UserService userService,
                           MailSenderService mailSenderService) {
        this.taskRepository = taskRepository;
        this.organizationRepository = organizationRepository;
        this.userService = userService;
        this.mailSenderService = mailSenderService;
    }

    @Override
    public TaskDto createTask(TaskDto request, String organizationId) {
        logger.debug("ActionLog.createTask.start");
        var organization = organizationRepository.findById(Long.parseLong(organizationId))
                .orElseThrow(() -> new RuntimeException(ORGANIZATION_NOT_FOUND));
        request.setOrganization(OrganizationMapper.INSTANCE.entityToDto(organization));
        var taskEntity = taskRepository.save(TaskMapper.INSTANCE.taskDtoToEntity(request));
        var task = TaskMapper.INSTANCE.taskEntityToDto(taskEntity);
        logger.debug("ActionLog.createTask.end");
        return task;
    }

    @Override
    public void deleteTaskByOrganization(Long id, String organizationId) {
        logger.debug("ActionLog.deleteTask.start by id :{}", id);
        var organization = organizationRepository.findById(Long.parseLong(organizationId))
                .orElseThrow(() -> new RuntimeException(ORGANIZATION_NOT_FOUND));
        taskRepository.deleteByIdAndOrganization(id, organization);
        logger.debug("ActionLog.deleteTask.end by id :{}", id);
    }

    @Override
    @Transactional
    public void deleteTask(Long id) {
        logger.debug("ActionLog.deleteTask.start by id :{}", id);
        taskRepository.deleteById(id);
        logger.debug("ActionLog.deleteTask.end by id :{}", id);
    }

    @Override
    public List<TaskDto> findAll() {
        logger.debug("ActionLog.findAll.start");
        var tasks = taskRepository.findAll()
                .stream()
                .map(TaskMapper.INSTANCE::taskEntityToDto)
                .collect(Collectors.toList());
        logger.debug("ActionLog.findAll.end");
        return tasks;
    }

    @Override
    public List<TaskDto> findTasksByOrganization(String organizationId) {
        logger.debug("ActionLog.findTasksByorganizationId.start");
        var organization = organizationRepository.findById(Long.parseLong(organizationId))
                .orElseThrow(() -> new RuntimeException(ORGANIZATION_NOT_FOUND));
        var tasks = taskRepository.findTasksByOrganization(organization).orElse(Collections.emptyList())
                .stream()
                .map(TaskMapper.INSTANCE::taskEntityToDto)
                .collect(Collectors.toList());
        logger.debug("ActionLog.findTasksByorganizationId.end");
        return tasks;
    }

    @Override
    public void setTaskCompleted(Long id) {
        logger.debug("ActionLog.setTaskCompleted.start by id:{}", id);
        TaskEntity task = taskRepository.findById(id).orElseThrow(() -> {
            throw new RuntimeException(TASK_NOT_FOUND);
        });
        task.setCompleted(true);
        taskRepository.save(task);
        logger.debug("ActionLog.setTaskCompleted.end by id:{}", id);
    }

    @Override
    public void setTaskNotCompleted(Long id) {
        logger.debug("ActionLog.setTaskNotCompleted.start by id:{}", id);
        TaskEntity task = taskRepository.findById(id).orElseThrow(() -> {
            throw new RuntimeException(TASK_NOT_FOUND);
        });
        task.setCompleted(false);
        taskRepository.save(task);
        logger.debug("ActionLog.setTaskNotCompleted.end by id:{}", id);
    }

    @Override
    public List<TaskDto> findFreeTasks(String organizationId) {
        var organization = organizationRepository.findById(Long.parseLong(organizationId))
                .orElseThrow(() -> new RuntimeException(ORGANIZATION_NOT_FOUND));
        return taskRepository.findTasksByOrganization(organization).orElse(Collections.emptyList())
                .stream()
                .filter(task -> task.getUsers() == null && !task.isCompleted())
                .map(TaskMapper.INSTANCE::taskEntityToDto).collect(Collectors.toList());
    }

    @Override
    public TaskDto getTaskById(Long id) {
        logger.debug("ActionLog.getTaskById.start by id:{}", id);
        var taskEntity = taskRepository.findById(id).orElseThrow(() -> {
            throw new RuntimeException(TASK_NOT_FOUND);
        });
        var task = TaskMapper.INSTANCE.taskEntityToDto(taskEntity);
        logger.debug("ActionLog.getTaskById.end by id:{}", id);
        return task;
    }

    @Override
    public TaskDto getTaskByIdAndOrganization(Long taskId, String organizationId) {
        var organization = organizationRepository.findById(Long.parseLong(organizationId))
                .orElseThrow(() -> new RuntimeException(ORGANIZATION_NOT_FOUND));
        logger.debug("ActionLog.getTaskById.start by id:{}", taskId);
        var taskEntity = taskRepository.findByIdAndOrganization(taskId, organization).orElseThrow(() -> {
            throw new RuntimeException(TASK_NOT_FOUND);
        });
        var task = TaskMapper.INSTANCE.taskEntityToDto(taskEntity);
        logger.debug("ActionLog.getTaskById.end by id:{}", taskId);
        return task;
    }

    @Override
    public void assignTaskToUser(Long taskId, Long userId) {
        logger.debug("ActionLog.assignTaskToUser.start by taskId:{}, userId:{}", taskId, userId);
        var task = taskRepository.findById(taskId).orElseThrow(() -> {
            throw new RuntimeException(TASK_NOT_FOUND);
        });
        logger.debug("ActionLog.getUserById id:{}", userId);
        var user = userService.getUserById(userId);
        if (task != null) {
            task.setUsers(Collections.singletonList(UserMapper.INSTANCE.userDtoToEntity(user)));
            taskRepository.save(task);
        }
        try {
            logger.debug("ActionLog.sendAssignTaskToUserMail");
            mailSenderService.sendMail(user.getEmail());
        } catch (Exception e) {
            logger.debug("ActionLog.sendAssignTaskToUserMail.error userId:{}, error:{}", userId, e.getMessage());
        }
        logger.debug("ActionLog.assignTaskToUser.end by taskId:{}, userId:{}", taskId, userId);
    }
}
