package com.etaskify.service.impl;

import com.etaskify.config.security.jwt.JwtUtils;
import com.etaskify.config.security.services.UserDetailsImpl;
import com.etaskify.dao.OrganizationEntity;
import com.etaskify.dao.OrganizationRepository;
import com.etaskify.dao.RoleRepository;
import com.etaskify.dao.UserEntity;
import com.etaskify.dao.UserRepository;
import com.etaskify.dto.CookieKeys;
import com.etaskify.dto.request.LoginRequest;
import com.etaskify.dto.request.SignupRequest;
import com.etaskify.dto.response.JwtResponse;
import com.etaskify.dto.response.MessageResponse;
import com.etaskify.service.AuthService;
import com.etaskify.service.UserService;
import com.etaskify.util.CookieHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import static com.etaskify.dto.Constants.EXISTS_BY_EMAIL;
import static com.etaskify.dto.Constants.EXISTS_BY_USERNAME;
import static com.etaskify.dto.Constants.ROLE_NOT_FOUND;
import static com.etaskify.dto.Constants.SUCCESSFULLY_REGISTERED;

@Service
public class AuthServiceImpl implements AuthService {
    private final Logger logger = LoggerFactory.getLogger(AuthServiceImpl.class);
    private final RoleRepository roleRepository;
    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final OrganizationRepository organizationRepository;
    private final PasswordEncoder encoder;
    private final CookieHelper cookieHelper;
    private final JwtUtils jwtUtils;
    private static final int COOKIE_TIMEOUT_SECONDS = 600;
    private static final String ADMIN = "ROLE_ADMIN";

    public AuthServiceImpl(RoleRepository roleRepository,
                           UserService userService,
                           AuthenticationManager authenticationManager,
                           UserRepository userRepository,
                           OrganizationRepository organizationRepository,
                           PasswordEncoder encoder,
                           CookieHelper cookieHelper,
                           JwtUtils jwtUtils) {
        this.roleRepository = roleRepository;
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.organizationRepository = organizationRepository;
        this.encoder = encoder;
        this.cookieHelper = cookieHelper;
        this.jwtUtils = jwtUtils;
    }

    @Override
    public JwtResponse authenticateUser(LoginRequest loginRequest, HttpServletResponse response) {
        logger.debug("ActionLog.authenticateUser.start");
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        var organization = userService.getUserById(userDetails.getId()).getOrganization();
        cookieHelper.addCookie(CookieKeys.ORGANIZATION_ID, organization.getId().toString(),
                COOKIE_TIMEOUT_SECONDS, response);
        var jwtResponse = JwtResponse.builder()
                .token(jwt)
                .id(userDetails.getId())
                .username(userDetails.getUsername())
                .email(userDetails.getEmail())
                .roles(roles)
                .build();
        logger.debug("ActionLog.authenticateUser.end");
        return jwtResponse;
    }

    @Override
    public MessageResponse registerUser(@Valid SignupRequest signUpRequest) {
        logger.debug("ActionLog.registerUser.end");
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new MessageResponse(EXISTS_BY_USERNAME);
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new MessageResponse(EXISTS_BY_EMAIL);
        }

        var organization = OrganizationEntity.builder()
                .name(signUpRequest.getOrganizationName())
                .phoneNumber(signUpRequest.getOrganizationPhoneNumber())
                .address(signUpRequest.getOrganizationAddress())
                .build();
        organization = organizationRepository.save(organization);
        var user = UserEntity.builder()
                .username(signUpRequest.getUsername())
                .email(signUpRequest.getEmail())
                .name(signUpRequest.getName())
                .surname(signUpRequest.getSurname())
                .password(encoder.encode(signUpRequest.getPassword()))
                .organization(organization)
                .build();

        var adminRole = roleRepository.findByName(ADMIN)
                .orElseThrow(() -> new RuntimeException(ROLE_NOT_FOUND));
        user.setRole(adminRole);
        userRepository.save(user);
        logger.debug("ActionLog.registerUser.end");
        return new MessageResponse(SUCCESSFULLY_REGISTERED);
    }
}

