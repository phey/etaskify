package com.etaskify.service.impl;

import com.etaskify.service.MailSenderService;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.SendGrid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class MailSenderServiceImpl implements MailSenderService {

    private final Logger logger = LoggerFactory.getLogger(MailSenderServiceImpl.class);
    private String defaultMailFrom;
    private String defaultMailSubject;
    private String defaultMailBody;
    private String defaultMailApiKey;

    public MailSenderServiceImpl(@Value("${default.mail.from}") String defaultMailFrom,
                                 @Value("${default.mail.subject}") String defaultMailSubject,
                                 @Value("${default.mail.body}") String defaultMailValue,
                                 @Value("${default.mail.apiKey}") String defaultMailApiKey) {
        this.defaultMailFrom = defaultMailFrom;
        this.defaultMailSubject = defaultMailSubject;
        this.defaultMailBody = defaultMailValue;
        this.defaultMailApiKey = defaultMailApiKey;
    }

    @Override
    public void sendMail(String emailAddress) {
        logger.debug("ActionLog.sendMail.start");
        Mail mail = new Mail(new Email(defaultMailFrom),
                defaultMailSubject,
                new Email(emailAddress),
                new Content("text/plain", defaultMailBody));
        SendGrid sg = new SendGrid(defaultMailApiKey);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            var response = sg.api(request);
            logger.debug("SG response status code:{}", response.getStatusCode());

        } catch (IOException ex) {
            logger.debug("ActionLog.sendMail.error: {}", ex.getMessage());
        }
        logger.debug("ActionLog.sendMail.end");
    }


}

