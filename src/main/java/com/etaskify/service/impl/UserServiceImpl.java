package com.etaskify.service.impl;

import com.etaskify.dao.OrganizationRepository;
import com.etaskify.dao.RoleRepository;
import com.etaskify.dao.UserEntity;
import com.etaskify.dao.UserRepository;
import com.etaskify.dto.UserDto;
import com.etaskify.dto.request.CreateUserRequest;
import com.etaskify.dto.response.MessageResponse;
import com.etaskify.mapper.UserMapper;
import com.etaskify.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.etaskify.dto.Constants.DEFAULT_PASSWORD;
import static com.etaskify.dto.Constants.EXISTS_BY_EMAIL;
import static com.etaskify.dto.Constants.EXISTS_BY_USERNAME;
import static com.etaskify.dto.Constants.ORGANIZATION_NOT_FOUND;
import static com.etaskify.dto.Constants.ROLE_NOT_FOUND;
import static com.etaskify.dto.Constants.SUCCESSFULLY_REGISTERED;
import static com.etaskify.dto.Constants.USERS_NOT_FOUND;
import static com.etaskify.dto.Constants.USER_NOT_FOUND;

@Service
public class UserServiceImpl implements UserService {
    private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private static final String USER = "ROLE_USER";

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final OrganizationRepository organizationRepository;
    private final PasswordEncoder encoder;

    public UserServiceImpl(UserRepository userRepository,
                           RoleRepository roleRepository,
                           OrganizationRepository organizationRepository,
                           PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.organizationRepository = organizationRepository;
        this.encoder = encoder;
    }

    @Override
    public MessageResponse createUser(CreateUserRequest request, String organizationId) {
        logger.debug("ActionLog.createUser.start");
        if (userRepository.existsByUsername(request.getUsername())) {
            return new MessageResponse(EXISTS_BY_USERNAME);
        }

        if (userRepository.existsByEmail(request.getEmail())) {
            return new MessageResponse(EXISTS_BY_EMAIL);
        }
        var organization = organizationRepository.findById(Long.parseLong(organizationId))
                .orElseThrow(() -> new RuntimeException(ORGANIZATION_NOT_FOUND));
        var userEntity = UserMapper.INSTANCE.createUserRequestToEntity(request);
        userEntity.setOrganization(organization);
        userEntity.setRole(roleRepository.findByName(USER)
                .orElseThrow(() -> new RuntimeException(ROLE_NOT_FOUND)));
        userEntity.setPassword(encoder.encode(DEFAULT_PASSWORD));
        userEntity = userRepository.save(userEntity);
        var user = UserMapper.INSTANCE.userEntityToDto(userEntity);
        logger.debug("ActionLog.createUser.end");
        return new MessageResponse(SUCCESSFULLY_REGISTERED);
    }

    @Override
    public List<UserDto> getUsersByOrganization(String organizationId) {
        var organization = organizationRepository.findById(Long.parseLong(organizationId))
                .orElseThrow(() -> new RuntimeException(ORGANIZATION_NOT_FOUND));
        var users = userRepository.findUsersByOrganization(organization)
                .orElseThrow(() -> new RuntimeException(USERS_NOT_FOUND));
        return users.stream().map(UserMapper.INSTANCE::userEntityToDto).collect(Collectors.toList());
    }

    @Override
    public List<UserDto> findAll() {
        logger.debug("ActionLog.findAllUsers.start");
        var users = userRepository.findAll().stream()
                .map(UserMapper.INSTANCE::userEntityToDto).collect(Collectors.toList());
        logger.debug("ActionLog.findAllUsers.end");
        return users;
    }

    @Override
    public UserDto getUserById(Long id) {
        logger.debug("ActionLog.getUserById.start id:{}", id);
        var userEntity = userRepository.findById(id).orElseThrow(() -> {
            throw new RuntimeException(USER_NOT_FOUND);
        });
        var user = UserMapper.INSTANCE.userEntityToDto(userEntity);
        logger.debug("ActionLog.getUserById.end id:{}", id);
        return user;
    }

    @Override
    public UserDto getUserByIdAndOrganization(Long userId, String organizationId) {
        logger.debug("ActionLog.getUserById.start id:{}", userId);
        var organization = organizationRepository.findById(Long.parseLong(organizationId))
                .orElseThrow(() -> new RuntimeException(ORGANIZATION_NOT_FOUND));
        var userEntity = userRepository.findByIdAndOrganization(userId, organization).orElseThrow(() -> {
            throw new RuntimeException(USER_NOT_FOUND);
        });
        var user = UserMapper.INSTANCE.userEntityToDto(userEntity);
        logger.debug("ActionLog.getUserById.end id:{}", userId);
        return user;
    }

    @Override
    public void deleteUser(Long id) {
        logger.debug("ActionLog.deleteUser.start by id :{}", id);
        UserEntity user = userRepository.findById(id).orElseThrow(() -> {
            throw new RuntimeException(USER_NOT_FOUND);
        });
        userRepository.delete(user);
        logger.debug("ActionLog.deleteUser.end by id :{}", id);
    }

    @Override
    public void deleteUserByOrganization(Long id, String organizationId) {
        logger.debug("ActionLog.deleteUser.start by id :{}", id);
        var organization = organizationRepository.findById(Long.parseLong(organizationId))
                .orElseThrow(() -> new RuntimeException(ORGANIZATION_NOT_FOUND));
        UserEntity user = userRepository.findByIdAndOrganization(id, organization).orElseThrow(() -> {
            throw new RuntimeException(USER_NOT_FOUND);
        });
        userRepository.delete(user);
        logger.debug("ActionLog.deleteUser.end by id :{}", id);
    }

}

