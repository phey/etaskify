package com.etaskify.service;

import com.etaskify.dto.TaskDto;

import java.util.List;

public interface TaskService {

    TaskDto createTask(TaskDto request, String organizationId);

    void deleteTaskByOrganization(Long id, String organizationId);

    void deleteTask(Long id);

    List<TaskDto> findAll();

    List<TaskDto> findTasksByOrganization(String organizationId);

    void setTaskCompleted(Long id);

    void setTaskNotCompleted(Long id);

    List<TaskDto> findFreeTasks(String organizationId);

    TaskDto getTaskById(Long taskId);

    TaskDto getTaskByIdAndOrganization(Long taskId, String organizationId);

    void assignTaskToUser(Long taskId, Long userId);
}
