package com.etaskify.mapper;

import com.etaskify.dao.UserEntity;
import com.etaskify.dto.UserDto;
import com.etaskify.dto.request.CreateUserRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class UserMapper {

    public static final UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "password", ignore = true)
    public abstract UserEntity userDtoToEntity(UserDto dto);

    public abstract UserDto userEntityToDto(UserEntity entity);

    @Mapping(target = "organization", ignore = true)
    @Mapping(target = "role", ignore = true)
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "id", ignore = true)
    public abstract UserEntity createUserRequestToEntity(CreateUserRequest user);

}
