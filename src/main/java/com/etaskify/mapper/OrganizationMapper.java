package com.etaskify.mapper;

import com.etaskify.dao.OrganizationEntity;
import com.etaskify.dto.OrganizationDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class OrganizationMapper {

    public static final OrganizationMapper INSTANCE = Mappers.getMapper(OrganizationMapper.class);

    public abstract OrganizationEntity dtoToEntity(OrganizationDto dto);

    public abstract OrganizationDto entityToDto(OrganizationEntity entity);
}
