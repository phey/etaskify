package com.etaskify.mapper;

import com.etaskify.dao.TaskEntity;
import com.etaskify.dto.TaskDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class TaskMapper {

    public static final TaskMapper INSTANCE = Mappers.getMapper(TaskMapper.class);

    @Mapping(target = "completed", ignore = true)
    public abstract TaskEntity taskDtoToEntity(TaskDto dto);

    @Mapping(target = "completed", ignore = true)
    public abstract TaskDto taskEntityToDto(TaskEntity entity);
}
