package com.etaskify.mapper;

import com.etaskify.dao.RoleEntity;
import com.etaskify.dto.RoleDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class RoleMapper {

    public static final RoleMapper INSTANCE = Mappers.getMapper(RoleMapper.class);

    public abstract RoleEntity roleDtoToEntity(RoleDto dto);

    public abstract RoleDto roleEntityToDto(RoleEntity entity);
}
