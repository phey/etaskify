package com.etaskify.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Component
public class CookieHelper {

    private static final Logger logger = LoggerFactory.getLogger(CookieHelper.class);

    public void addCookie(String name, String value, int maxAge, HttpServletResponse response) {
        logger.debug("addCookie start, cookie {}", name);

        addCookieWithMaxAge(name, value, maxAge, response);

        logger.debug("addCookie end, cookie {}", name);
    }

    private void addCookieWithMaxAge(String name, String value, int maxAge, HttpServletResponse response) {
        Cookie cookie = new Cookie(name, value);
        cookie.setSecure(true);
        cookie.setHttpOnly(true);
        cookie.setMaxAge(maxAge);
        response.addCookie(cookie);
    }
}
