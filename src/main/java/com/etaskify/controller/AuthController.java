package com.etaskify.controller;

import com.etaskify.dto.request.LoginRequest;
import com.etaskify.dto.request.SignupRequest;
import com.etaskify.dto.response.JwtResponse;
import com.etaskify.dto.response.MessageResponse;
import com.etaskify.service.AuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/v1/auth")
@Api("Authentiction Controller")
public class AuthController {
    private final Logger logger = LoggerFactory.getLogger(AuthController.class);
    private final AuthService authService;


    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/signin")
    @ApiOperation(value = "Login with username and password")
    public JwtResponse authenticateUser(@Valid @RequestBody LoginRequest loginRequest,
                                        @ApiParam(hidden = true) @NotNull HttpServletResponse response) {
        logger.info("ActionLog.authenticateUser.start");
        var res = authService.authenticateUser(loginRequest, response);
        logger.info("ActionLog.authenticateUser.end");
        return res;
    }

    @PostMapping("/signup")
    @ApiOperation(value = "Register new admin user for new organization")
    public MessageResponse registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        logger.info("ActionLog.registerUser.start");
        var response = authService.registerUser(signUpRequest);
        logger.info("ActionLog.registerUser.start");
        return response;
    }
}
