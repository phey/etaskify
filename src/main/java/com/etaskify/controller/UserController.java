package com.etaskify.controller;

import com.etaskify.dto.UserDto;
import com.etaskify.dto.request.CreateUserRequest;
import com.etaskify.dto.response.MessageResponse;
import com.etaskify.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import javax.validation.Valid;

import static com.etaskify.dto.CookieKeys.ORGANIZATION_ID;

@RestController
@RequestMapping(path = "/v1/user")
@Api("Users Controller")
public class UserController {
    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Find all users")
    public List<UserDto> listUsers(@CookieValue(value = ORGANIZATION_ID) String organizationId) {
        logger.info("ActionLog.getUsersByOrganizationId.start");
        var users = userService.getUsersByOrganization(organizationId);
        logger.info("ActionLog.getUsersByOrganizationId.end");
        return users;
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Create new user(password:defaultPassword)")
    public MessageResponse createUser(@Valid @RequestBody CreateUserRequest request,
                                      @CookieValue(value = ORGANIZATION_ID) String organizationId) {
        logger.info("ActionLog.createUser.start");
        var resposne = userService.createUser(request, organizationId);
        logger.info("ActionLog.createUser.end");
        return resposne;
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Find user by id")
    public UserDto getUserById(@PathVariable("id") Long userId,
                               @CookieValue(value = ORGANIZATION_ID) String organizationId) {
        logger.info("ActionLog.getUserById id:{}", userId);
        return userService.getUserByIdAndOrganization(userId, organizationId);
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Delete user by id")
    public void deleteUser(@PathVariable Long id, @CookieValue(value = ORGANIZATION_ID) String organizationId) {
        logger.info("ActionLog.deleteUser.start");
        userService.deleteUserByOrganization(id, organizationId);
        logger.info("ActionLog.deleteUser.end");
    }

}
