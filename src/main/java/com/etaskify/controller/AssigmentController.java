package com.etaskify.controller;

import com.etaskify.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/v1/assignment")
@Api("Assigment Controller")
public class AssigmentController {

    private final Logger logger = LoggerFactory.getLogger(AssigmentController.class);
    private final TaskService taskService;

    @Autowired
    public AssigmentController(TaskService taskService) {
        this.taskService = taskService;
    }


    @GetMapping("/assign/{userId}/{taskId}")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Assign task to user")
    public void assignTaskToUser(@PathVariable Long userId, @PathVariable Long taskId) {
        logger.info("ActionLog.assignTaskToUser.start");
        taskService.assignTaskToUser(taskId, userId);
        logger.info("ActionLog.assignTaskToUser.end");
    }

}



