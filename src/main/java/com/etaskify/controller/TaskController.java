package com.etaskify.controller;

import com.etaskify.dto.TaskDto;
import com.etaskify.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import javax.validation.Valid;

import static com.etaskify.dto.CookieKeys.ORGANIZATION_ID;

@RestController
@RequestMapping(path = "/v1/task")
@Api("Task Controller")
public class TaskController {

    private final Logger logger = LoggerFactory.getLogger(TaskController.class);
    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Create new task")
    public TaskDto createTask(@Valid @RequestBody TaskDto request,
                              @CookieValue(value = ORGANIZATION_ID) String organizationId) {
        logger.info("ActionLog.createTask.start");
        logger.info("cookie value: {}", organizationId);
        var task = taskService.createTask(request, organizationId);
        logger.info("ActionLog.createTask.end");
        return task;
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Find task by id")
    public TaskDto getTaskById(@PathVariable Long id, @CookieValue(value = ORGANIZATION_ID) String organizationId) {
        logger.info("ActionLog.getTaskById id:{}", id);
        return taskService.getTaskByIdAndOrganization(id, organizationId);
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Delete task by id")
    public void deleteTaskByOrganization(@PathVariable Long id,
                                         @CookieValue(value = ORGANIZATION_ID) String organizationId) {
        logger.info("ActionLog.deleteTaskByOrganization.start");
        taskService.deleteTaskByOrganization(id, organizationId);
        logger.info("ActionLog.deleteTaskByOrganization.end");
    }

    @GetMapping("/mark-done/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Mark task is completed")
    public void setTaskCompleted(@PathVariable Long id) {
        logger.info("ActionLog.setTaskCompleted.start");
        taskService.setTaskCompleted(id);
        logger.info("ActionLog.setTaskCompleted.end");
    }

    @GetMapping("/unmark-done/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Mark task is not completed")
    public void setTaskNotCompleted(@PathVariable Long id) {
        logger.info("ActionLog.setTaskNotCompleted.start");
        taskService.setTaskNotCompleted(id);
        logger.info("ActionLog.setTaskNotCompleted.end");
    }

    @GetMapping("/unassigned")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Find all unassigned taska")
    public List<TaskDto> findFreeTasks(@CookieValue(value = ORGANIZATION_ID) String organizationId) {
        logger.info("ActionLog.findFreeTasks.start");
        var tasks = taskService.findFreeTasks(organizationId);
        logger.info("ActionLog.findFreeTasks.end");
        return tasks;
    }

    @GetMapping("/tasks")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Find all tasks")
    public List<TaskDto> findAllTasks(@CookieValue(value = ORGANIZATION_ID) String organizationId) {
        logger.info("ActionLog.findTasksByOrganizationId.start");
        var tasks = taskService.findTasksByOrganization(organizationId);
        logger.info("ActionLog.findTasksByOrganizationId.end");
        return tasks;
    }

}
