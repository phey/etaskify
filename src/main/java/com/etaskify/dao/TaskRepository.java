package com.etaskify.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<TaskEntity, Long> {

    Optional<List<TaskEntity>> findTasksByOrganization(OrganizationEntity organization);

    void deleteByIdAndOrganization(Long id, OrganizationEntity organization);

    Optional<TaskEntity> findByIdAndOrganization(Long id, OrganizationEntity organization);
}
