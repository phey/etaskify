package com.etaskify.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findByUsername(String username);

    Optional<UserEntity> findByIdAndOrganization(Long id, OrganizationEntity organization);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    Optional<List<UserEntity>> findUsersByOrganization(OrganizationEntity organization);
}
