package com.etaskify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableJpaRepositories
@RestController
public class Application {

    @GetMapping
    public String test(){
        return "successs";
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
