package com.etaskify.mapper;

import com.etaskify.dao.OrganizationEntity;
import com.etaskify.dao.TaskEntity;
import com.etaskify.dao.UserEntity;
import com.etaskify.dto.OrganizationDto;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-10-26T16:10:53+0400",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.3 (Oracle Corporation)"
)
public class OrganizationMapperImpl extends OrganizationMapper {

    @Override
    public OrganizationEntity dtoToEntity(OrganizationDto dto) {
        if ( dto == null ) {
            return null;
        }

        OrganizationEntity organizationEntity = new OrganizationEntity();

        organizationEntity.setId( dto.getId() );
        organizationEntity.setName( dto.getName() );
        organizationEntity.setPhoneNumber( dto.getPhoneNumber() );
        organizationEntity.setAddress( dto.getAddress() );
        Set<UserEntity> set = dto.getUsers();
        if ( set != null ) {
            organizationEntity.setUsers( new HashSet<UserEntity>( set ) );
        }
        Set<TaskEntity> set1 = dto.getTasks();
        if ( set1 != null ) {
            organizationEntity.setTasks( new HashSet<TaskEntity>( set1 ) );
        }

        return organizationEntity;
    }

    @Override
    public OrganizationDto entityToDto(OrganizationEntity entity) {
        if ( entity == null ) {
            return null;
        }

        OrganizationDto organizationDto = new OrganizationDto();

        organizationDto.setId( entity.getId() );
        organizationDto.setName( entity.getName() );
        organizationDto.setPhoneNumber( entity.getPhoneNumber() );
        organizationDto.setAddress( entity.getAddress() );
        Set<UserEntity> set = entity.getUsers();
        if ( set != null ) {
            organizationDto.setUsers( new HashSet<UserEntity>( set ) );
        }
        Set<TaskEntity> set1 = entity.getTasks();
        if ( set1 != null ) {
            organizationDto.setTasks( new HashSet<TaskEntity>( set1 ) );
        }

        return organizationDto;
    }
}
