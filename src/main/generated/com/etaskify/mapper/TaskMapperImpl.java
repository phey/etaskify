package com.etaskify.mapper;

import com.etaskify.dao.OrganizationEntity;
import com.etaskify.dao.RoleEntity;
import com.etaskify.dao.TaskEntity;
import com.etaskify.dao.UserEntity;
import com.etaskify.dto.OrganizationDto;
import com.etaskify.dto.RoleDto;
import com.etaskify.dto.TaskDto;
import com.etaskify.dto.UserDto;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-10-26T16:10:53+0400",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.3 (Oracle Corporation)"
)
public class TaskMapperImpl extends TaskMapper {

    @Override
    public TaskEntity taskDtoToEntity(TaskDto dto) {
        if ( dto == null ) {
            return null;
        }

        TaskEntity taskEntity = new TaskEntity();

        taskEntity.setId( dto.getId() );
        taskEntity.setTitle( dto.getTitle() );
        taskEntity.setDescription( dto.getDescription() );
        taskEntity.setDeadline( dto.getDeadline() );
        taskEntity.setUsers( userDtoListToUserEntityList( dto.getUsers() ) );
        taskEntity.setOrganization( organizationDtoToOrganizationEntity( dto.getOrganization() ) );

        return taskEntity;
    }

    @Override
    public TaskDto taskEntityToDto(TaskEntity entity) {
        if ( entity == null ) {
            return null;
        }

        TaskDto taskDto = new TaskDto();

        taskDto.setId( entity.getId() );
        taskDto.setTitle( entity.getTitle() );
        taskDto.setDescription( entity.getDescription() );
        taskDto.setDeadline( entity.getDeadline() );
        taskDto.setUsers( userEntityListToUserDtoList( entity.getUsers() ) );
        taskDto.setOrganization( organizationEntityToOrganizationDto( entity.getOrganization() ) );

        return taskDto;
    }

    protected OrganizationEntity organizationDtoToOrganizationEntity(OrganizationDto organizationDto) {
        if ( organizationDto == null ) {
            return null;
        }

        OrganizationEntity organizationEntity = new OrganizationEntity();

        organizationEntity.setId( organizationDto.getId() );
        organizationEntity.setName( organizationDto.getName() );
        organizationEntity.setPhoneNumber( organizationDto.getPhoneNumber() );
        organizationEntity.setAddress( organizationDto.getAddress() );
        Set<UserEntity> set = organizationDto.getUsers();
        if ( set != null ) {
            organizationEntity.setUsers( new HashSet<UserEntity>( set ) );
        }
        Set<TaskEntity> set1 = organizationDto.getTasks();
        if ( set1 != null ) {
            organizationEntity.setTasks( new HashSet<TaskEntity>( set1 ) );
        }

        return organizationEntity;
    }

    protected RoleEntity roleDtoToRoleEntity(RoleDto roleDto) {
        if ( roleDto == null ) {
            return null;
        }

        RoleEntity roleEntity = new RoleEntity();

        roleEntity.setId( roleDto.getId() );
        roleEntity.setName( roleDto.getName() );

        return roleEntity;
    }

    protected UserEntity userDtoToUserEntity(UserDto userDto) {
        if ( userDto == null ) {
            return null;
        }

        UserEntity userEntity = new UserEntity();

        userEntity.setId( userDto.getId() );
        userEntity.setEmail( userDto.getEmail() );
        userEntity.setName( userDto.getName() );
        userEntity.setSurname( userDto.getSurname() );
        userEntity.setUsername( userDto.getUsername() );
        userEntity.setOrganization( organizationDtoToOrganizationEntity( userDto.getOrganization() ) );
        userEntity.setRole( roleDtoToRoleEntity( userDto.getRole() ) );

        return userEntity;
    }

    protected List<UserEntity> userDtoListToUserEntityList(List<UserDto> list) {
        if ( list == null ) {
            return null;
        }

        List<UserEntity> list1 = new ArrayList<UserEntity>( list.size() );
        for ( UserDto userDto : list ) {
            list1.add( userDtoToUserEntity( userDto ) );
        }

        return list1;
    }

    protected OrganizationDto organizationEntityToOrganizationDto(OrganizationEntity organizationEntity) {
        if ( organizationEntity == null ) {
            return null;
        }

        OrganizationDto organizationDto = new OrganizationDto();

        organizationDto.setId( organizationEntity.getId() );
        organizationDto.setName( organizationEntity.getName() );
        organizationDto.setPhoneNumber( organizationEntity.getPhoneNumber() );
        organizationDto.setAddress( organizationEntity.getAddress() );
        Set<UserEntity> set = organizationEntity.getUsers();
        if ( set != null ) {
            organizationDto.setUsers( new HashSet<UserEntity>( set ) );
        }
        Set<TaskEntity> set1 = organizationEntity.getTasks();
        if ( set1 != null ) {
            organizationDto.setTasks( new HashSet<TaskEntity>( set1 ) );
        }

        return organizationDto;
    }

    protected RoleDto roleEntityToRoleDto(RoleEntity roleEntity) {
        if ( roleEntity == null ) {
            return null;
        }

        RoleDto roleDto = new RoleDto();

        roleDto.setId( roleEntity.getId() );
        roleDto.setName( roleEntity.getName() );

        return roleDto;
    }

    protected UserDto userEntityToUserDto(UserEntity userEntity) {
        if ( userEntity == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setId( userEntity.getId() );
        userDto.setEmail( userEntity.getEmail() );
        userDto.setName( userEntity.getName() );
        userDto.setSurname( userEntity.getSurname() );
        userDto.setUsername( userEntity.getUsername() );
        userDto.setOrganization( organizationEntityToOrganizationDto( userEntity.getOrganization() ) );
        userDto.setRole( roleEntityToRoleDto( userEntity.getRole() ) );

        return userDto;
    }

    protected List<UserDto> userEntityListToUserDtoList(List<UserEntity> list) {
        if ( list == null ) {
            return null;
        }

        List<UserDto> list1 = new ArrayList<UserDto>( list.size() );
        for ( UserEntity userEntity : list ) {
            list1.add( userEntityToUserDto( userEntity ) );
        }

        return list1;
    }
}
