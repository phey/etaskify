package com.etaskify.mapper;

import com.etaskify.dao.RoleEntity;
import com.etaskify.dto.RoleDto;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-10-26T16:10:53+0400",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.3 (Oracle Corporation)"
)
public class RoleMapperImpl extends RoleMapper {

    @Override
    public RoleEntity roleDtoToEntity(RoleDto dto) {
        if ( dto == null ) {
            return null;
        }

        RoleEntity roleEntity = new RoleEntity();

        roleEntity.setId( dto.getId() );
        roleEntity.setName( dto.getName() );

        return roleEntity;
    }

    @Override
    public RoleDto roleEntityToDto(RoleEntity entity) {
        if ( entity == null ) {
            return null;
        }

        RoleDto roleDto = new RoleDto();

        roleDto.setId( entity.getId() );
        roleDto.setName( entity.getName() );

        return roleDto;
    }
}
