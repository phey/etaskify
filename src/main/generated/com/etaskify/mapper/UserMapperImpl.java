package com.etaskify.mapper;

import com.etaskify.dao.OrganizationEntity;
import com.etaskify.dao.RoleEntity;
import com.etaskify.dao.TaskEntity;
import com.etaskify.dao.UserEntity;
import com.etaskify.dto.OrganizationDto;
import com.etaskify.dto.RoleDto;
import com.etaskify.dto.UserDto;
import com.etaskify.dto.request.CreateUserRequest;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-10-26T16:10:53+0400",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.3 (Oracle Corporation)"
)
public class UserMapperImpl extends UserMapper {

    @Override
    public UserEntity userDtoToEntity(UserDto dto) {
        if ( dto == null ) {
            return null;
        }

        UserEntity userEntity = new UserEntity();

        userEntity.setId( dto.getId() );
        userEntity.setEmail( dto.getEmail() );
        userEntity.setName( dto.getName() );
        userEntity.setSurname( dto.getSurname() );
        userEntity.setUsername( dto.getUsername() );
        userEntity.setOrganization( organizationDtoToOrganizationEntity( dto.getOrganization() ) );
        userEntity.setRole( roleDtoToRoleEntity( dto.getRole() ) );

        return userEntity;
    }

    @Override
    public UserDto userEntityToDto(UserEntity entity) {
        if ( entity == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setId( entity.getId() );
        userDto.setEmail( entity.getEmail() );
        userDto.setName( entity.getName() );
        userDto.setSurname( entity.getSurname() );
        userDto.setUsername( entity.getUsername() );
        userDto.setOrganization( organizationEntityToOrganizationDto( entity.getOrganization() ) );
        userDto.setRole( roleEntityToRoleDto( entity.getRole() ) );

        return userDto;
    }

    @Override
    public UserEntity createUserRequestToEntity(CreateUserRequest user) {
        if ( user == null ) {
            return null;
        }

        UserEntity userEntity = new UserEntity();

        userEntity.setEmail( user.getEmail() );
        userEntity.setName( user.getName() );
        userEntity.setSurname( user.getSurname() );
        userEntity.setUsername( user.getUsername() );

        return userEntity;
    }

    protected OrganizationEntity organizationDtoToOrganizationEntity(OrganizationDto organizationDto) {
        if ( organizationDto == null ) {
            return null;
        }

        OrganizationEntity organizationEntity = new OrganizationEntity();

        organizationEntity.setId( organizationDto.getId() );
        organizationEntity.setName( organizationDto.getName() );
        organizationEntity.setPhoneNumber( organizationDto.getPhoneNumber() );
        organizationEntity.setAddress( organizationDto.getAddress() );
        Set<UserEntity> set = organizationDto.getUsers();
        if ( set != null ) {
            organizationEntity.setUsers( new HashSet<UserEntity>( set ) );
        }
        Set<TaskEntity> set1 = organizationDto.getTasks();
        if ( set1 != null ) {
            organizationEntity.setTasks( new HashSet<TaskEntity>( set1 ) );
        }

        return organizationEntity;
    }

    protected RoleEntity roleDtoToRoleEntity(RoleDto roleDto) {
        if ( roleDto == null ) {
            return null;
        }

        RoleEntity roleEntity = new RoleEntity();

        roleEntity.setId( roleDto.getId() );
        roleEntity.setName( roleDto.getName() );

        return roleEntity;
    }

    protected OrganizationDto organizationEntityToOrganizationDto(OrganizationEntity organizationEntity) {
        if ( organizationEntity == null ) {
            return null;
        }

        OrganizationDto organizationDto = new OrganizationDto();

        organizationDto.setId( organizationEntity.getId() );
        organizationDto.setName( organizationEntity.getName() );
        organizationDto.setPhoneNumber( organizationEntity.getPhoneNumber() );
        organizationDto.setAddress( organizationEntity.getAddress() );
        Set<UserEntity> set = organizationEntity.getUsers();
        if ( set != null ) {
            organizationDto.setUsers( new HashSet<UserEntity>( set ) );
        }
        Set<TaskEntity> set1 = organizationEntity.getTasks();
        if ( set1 != null ) {
            organizationDto.setTasks( new HashSet<TaskEntity>( set1 ) );
        }

        return organizationDto;
    }

    protected RoleDto roleEntityToRoleDto(RoleEntity roleEntity) {
        if ( roleEntity == null ) {
            return null;
        }

        RoleDto roleDto = new RoleDto();

        roleDto.setId( roleEntity.getId() );
        roleDto.setName( roleEntity.getName() );

        return roleDto;
    }
}
