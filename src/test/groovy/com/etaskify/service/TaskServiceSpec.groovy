package com.etaskify.service

import com.etaskify.dao.OrganizationEntity
import com.etaskify.dao.OrganizationRepository
import com.etaskify.dao.TaskEntity
import com.etaskify.dao.TaskRepository
import com.etaskify.dto.Constants
import com.etaskify.service.impl.TaskServiceImpl
import spock.lang.Specification

class TaskServiceSpec extends Specification {

    private TaskRepository taskRepository
    private OrganizationRepository organizationRepository
    private UserService userService
    private MailSenderService mailSenderService
    private TaskService taskService

    def setup() {
        taskRepository = Mock()
        organizationRepository = Mock()
        userService = Mock()
        mailSenderService = Mock()

        taskService = new TaskServiceImpl(taskRepository, organizationRepository, userService, mailSenderService)
    }

    def "getTaskById success"() {
        given:
        def entity = TaskEntity.builder()
                .id(1L)
                .title("title")
                .build()

        when:
        def actual = taskService.getTaskById(1L)

        then:
        1 * taskRepository.findById(1L) >> Optional.of(entity)
        actual.title == "title"
    }

    def "getUserById task not found"() {
        when:
        taskService.getTaskById(1L)

        then:
        1 * taskRepository.findById(1L) >> Optional.empty()
        RuntimeException ex = thrown()
        ex.getMessage() == Constants.TASK_NOT_FOUND
    }

    def "getTaskByIdAndorganization success"() {
        given:
        def organization = OrganizationEntity.builder().id(1L).build()
        def entity = TaskEntity.builder()
                .id(1L)
                .title("title")
                .build()

        when:
        def actual = taskService.getTaskByIdAndOrganization(1L, "1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.of(organization)
        1 * taskRepository.findByIdAndOrganization(1L, organization) >> Optional.of(entity)
        actual.title == "title"
    }

    def "getTaskByIdAndorganization task not found"() {
        given:
        def organization = OrganizationEntity.builder().id(1L).build()

        when:
        taskService.getTaskByIdAndOrganization(1L, "1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.of(organization)
        1 * taskRepository.findByIdAndOrganization(1L, organization) >> Optional.empty()
        RuntimeException ex = thrown()
        ex.getMessage() == Constants.TASK_NOT_FOUND
    }

    def "findAll success"() {
        given:
        def entity = TaskEntity.builder()
                .title("title")
                .build()
        when:
        def list = taskService.findAll()

        then:
        1 * taskRepository.findAll() >> [entity]
        list.get(0).title == "title"
    }

    def "delete task success"() {
        when:
        taskService.deleteTask(1L)

        then:
        1 * taskRepository.deleteById(1L)
    }

    def "delete task by organization success"() {
        given:
        def organization = OrganizationEntity.builder().id(1L).build()

        when:
        taskService.deleteTaskByOrganization(1L, "1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.of(organization)
        1 * taskRepository.deleteByIdAndOrganization(1L, organization)
    }

    def "deleteTask by organization -- organization not found"() {
        when:
        taskService.deleteTaskByOrganization(1L, "1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.empty()
        RuntimeException ex = thrown()
        ex.getMessage() == Constants.ORGANIZATION_NOT_FOUND
    }

    def "findTasksByorganizationId success"() {
        given:
        def organization = OrganizationEntity.builder().id(1L).build()
        def entity = TaskEntity.builder()
                .id(1)
                .title("title")
                .organization(organization)
                .build()
        when:
        def actual = taskService.getTaskByIdAndOrganization(1L, "1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.of(organization)
        1 * taskRepository.findByIdAndOrganization(1L, organization) >> Optional.of(entity)
        actual.title == "title"
    }

    def "findTasksByorganizationId exception organization not found"() {
        when:
        taskService.getTaskByIdAndOrganization(1L, "1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.empty()
        RuntimeException ex = thrown()
        ex.getMessage() == Constants.ORGANIZATION_NOT_FOUND
    }

    def "setTaskCompleted success"() {
        given:
        def entity = TaskEntity.builder()
                .id(1)
                .completed(false)
                .title("title")
                .build()

        when:
        taskService.setTaskCompleted(1L)

        then:
        1 * taskRepository.findById(1L) >> Optional.of(entity)
        1 * taskRepository.save(entity)
    }

    def "setTaskCompleted task not found"() {
        when:
        taskService.setTaskCompleted(1L)

        then:
        1 * taskRepository.findById(1L) >> Optional.empty()
        RuntimeException ex = thrown()
        ex.getMessage() == Constants.TASK_NOT_FOUND
    }

    def "setTaskNotCompleted success"() {
        given:
        def entity = TaskEntity.builder()
                .id(1)
                .completed(true)
                .title("title")
                .build()

        when:
        taskService.setTaskCompleted(1L)

        then:
        1 * taskRepository.findById(1L) >> Optional.of(entity)
        1 * taskRepository.save(entity)
    }

    def "setTaskNotCompleted task not found"() {
        when:
        taskService.setTaskCompleted(1L)

        then:
        1 * taskRepository.findById(1L) >> Optional.empty()
        RuntimeException ex = thrown()
        ex.getMessage() == Constants.TASK_NOT_FOUND
    }

    def "find all free tasks"() {
        given:
        def organization = OrganizationEntity.builder().id(1L).build()
        def entity = TaskEntity.builder()
                .title("title")
                .build()
        when:
        def list = taskService.findFreeTasks("1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.of(organization)
        1 * taskRepository.findTasksByOrganization(organization) >> Optional.of([entity])
        list.get(0).title == "title"
    }

    def "find all free tasks organization not found"() {
        when:
        taskService.findFreeTasks("1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.empty()
        RuntimeException ex = thrown()
        ex.getMessage() == Constants.ORGANIZATION_NOT_FOUND
    }

    def "assignTaskToUser success"() {
        given:
        def entity = TaskEntity.builder()
                .id(1)
                .completed(false)
                .title("title")
                .build()

        when:
        taskService.assignTaskToUser(1L, 1L)

        then:
        1 * taskRepository.findById(1L) >> Optional.of(entity)
        1 * taskRepository.save(entity)
        1 * userService.getUserById(1L)
    }

    def "assignTaskToUser task not found"() {
        when:
        taskService.assignTaskToUser(1L, 1L)

        then:
        1 * taskRepository.findById(1L) >> Optional.empty()
        RuntimeException ex = thrown()
        ex.getMessage() == Constants.TASK_NOT_FOUND
    }
}
