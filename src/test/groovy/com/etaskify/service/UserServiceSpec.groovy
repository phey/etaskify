package com.etaskify.service

import com.etaskify.dao.*
import com.etaskify.dto.Constants
import com.etaskify.dto.request.CreateUserRequest
import com.etaskify.service.impl.UserServiceImpl
import org.springframework.security.crypto.password.PasswordEncoder
import spock.lang.Specification

class UserServiceSpec extends Specification {
    private UserRepository userRepository
    private RoleRepository roleRepository
    private OrganizationRepository organizationRepository
    private PasswordEncoder encoder
    private UserService userService

    def setup() {
        userRepository = Mock()
        organizationRepository = Mock()
        roleRepository = Mock()
        encoder = Mock()

        userService = new UserServiceImpl(userRepository, roleRepository, organizationRepository, encoder)
    }

    def "createUser success"() {
        given:
        def request = CreateUserRequest.builder()
                .username("username")
                .email("email")
                .name("name")
                .surname("surname")
                .build()
        def organization = OrganizationEntity.builder().id(1L).build()
        def role = RoleEntity.builder().build()

        when:
        def response = userService.createUser(request, "1")

        then:
        1 * userRepository.existsByUsername(request.getUsername()) >> false
        1 * userRepository.existsByEmail(request.getEmail()) >> false
        1 * organizationRepository.findById(1L) >> Optional.of(organization)
        1 * roleRepository.findByName("ROLE_USER") >> Optional.of(role)
        1 * encoder.encode('defaultPassword')
        response.message == Constants.SUCCESSFULLY_REGISTERED
    }

    def "createUser username already used"() {
        given:
        def request = CreateUserRequest.builder()
                .username("username")
                .email("email")
                .name("name")
                .surname("surname")
                .build()

        when:
        def response = userService.createUser(request, "1")

        then:
        1 * userRepository.existsByUsername(request.getUsername()) >> true
        response.message == Constants.EXISTS_BY_USERNAME
    }

    def "createUser email already used"() {
        given:
        def request = CreateUserRequest.builder()
                .username("username")
                .email("email")
                .name("name")
                .surname("surname")
                .build()

        when:
        def response = userService.createUser(request, "1")

        then:
        1 * userRepository.existsByUsername(request.getUsername()) >> false
        1 * userRepository.existsByEmail(request.getEmail()) >> true
        response.message == Constants.EXISTS_BY_EMAIL
    }

    def "getUserById success"() {
        given:
        def entity = UserEntity.builder()
                .id(1L)
                .username("username")
                .build()

        when:
        def actual = userService.getUserById(1L)

        then:
        1 * userRepository.findById(1L) >> Optional.of(entity)
        actual.username == "username"
    }

    def "getUserById user not found"() {

        when:
        userService.getUserById(1L)

        then:
        1 * userRepository.findById(1L) >> Optional.empty()
        RuntimeException ex = thrown()
        ex.getMessage() == Constants.USER_NOT_FOUND
    }

    def "getUserByIdAndOrganization success"() {
        given:
        def organization = OrganizationEntity.builder().id(1L).build()
        def entity = UserEntity.builder()
                .id(1L)
                .username("username")
                .build()

        when:
        def actual = userService.getUserByIdAndOrganization(1L, "1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.of(organization)
        1 * userRepository.findByIdAndOrganization(1L, organization) >> Optional.of(entity)
        actual.username == "username"
    }

    def "getUserByIdAndOrganization user not found"() {
        given:
        def organization = OrganizationEntity.builder().id(1L).build()

        when:
        userService.getUserByIdAndOrganization(1L, "1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.of(organization)
        1 * userRepository.findByIdAndOrganization(1L, organization) >> Optional.empty()
        RuntimeException ex = thrown()
        ex.getMessage() == Constants.USER_NOT_FOUND
    }

    def "findAll success"() {
        given:
        def entity = UserEntity.builder()
                .username("username")
                .build()
        when:
        def list = userService.findAll()

        then:
        1 * userRepository.findAll() >> [entity]
        list.get(0).username == "username"
    }

    def "delete user success"() {
        given:
        def entity = UserEntity.builder()
                .username("username")
                .build()
        when:
        userService.deleteUser(1L)

        then:
        1 * userRepository.findById(1L) >> Optional.of(entity)
    }

    def "deleteUser user not found"() {

        when:
        userService.deleteUser(1L)

        then:
        1 * userRepository.findById(1L) >> Optional.empty()
        RuntimeException ex = thrown()
        ex.getMessage() == Constants.USER_NOT_FOUND
    }

    def "delete user by organization success"() {
        given:
        def organization = OrganizationEntity.builder().id(1L).build()
        def entity = UserEntity.builder()
                .username("username")
                .build()
        when:
        userService.deleteUserByOrganization(1L, "1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.of(organization)
        1 * userRepository.findByIdAndOrganization(1L, organization) >> Optional.of(entity)
    }

    def "deleteUser by organization user not found"() {
        given:
        def organization = OrganizationEntity.builder().id(1L).build()

        when:
        userService.deleteUserByOrganization(1L, "1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.of(organization)
        1 * userRepository.findByIdAndOrganization(1L, organization) >> Optional.empty()
        RuntimeException ex = thrown()
        ex.getMessage() == Constants.USER_NOT_FOUND
    }

    def "getUsersByOrganizationId success"() {
        given:
        def organization = OrganizationEntity.builder().id(1L).build()
        def entity = UserEntity.builder()
                .username("username")
                .organization(organization)
                .build()
        when:
        def list = userService.getUsersByOrganization("1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.of(organization)
        1 * userRepository.findUsersByOrganization(organization) >> Optional.of([entity])
        list.get(0).username == "username"
    }

    def "getUsersByOrganizationId exception organization not found"() {
        when:
        userService.getUsersByOrganization("1")

        then:
        1 * organizationRepository.findById(1L) >> Optional.empty()
        RuntimeException ex = thrown()
        ex.getMessage() == Constants.ORGANIZATION_NOT_FOUND
    }
}
