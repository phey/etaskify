package com.etaskify.mapper


import com.etaskify.dao.OrganizationEntity
import com.etaskify.dto.OrganizationDto
import io.github.benas.randombeans.api.EnhancedRandom
import spock.lang.Specification


class OrganizationMapperSpec extends Specification {

    def "Test convert dto to entity"() {
        given:
        def dto = EnhancedRandom.random(OrganizationDto.class)
        dto.id = 1
        when:
        def entity = OrganizationMapper.INSTANCE.dtoToEntity(dto)

        then:
        entity.id == dto.id
        entity.name == dto.name
        entity.address == dto.address
        entity.phoneNumber == dto.phoneNumber
        entity.users == dto.users
        entity.tasks == dto.tasks
    }

    def "Test convert entity to dto"() {
        given:
        def entity = EnhancedRandom.random(OrganizationEntity.class)
        entity.id = 1
        when:
        def dto = OrganizationMapper.INSTANCE.entityToDto(entity)

        then:
        dto.id == entity.id
        dto.name == entity.name
        dto.address == entity.address
        dto.phoneNumber == entity.phoneNumber
        dto.users == entity.users
        dto.tasks == entity.tasks
    }

}
