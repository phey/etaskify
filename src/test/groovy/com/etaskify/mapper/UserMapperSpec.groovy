package com.etaskify.mapper

import com.etaskify.dao.UserEntity
import com.etaskify.dto.UserDto
import com.etaskify.dto.request.CreateUserRequest
import io.github.benas.randombeans.api.EnhancedRandom
import spock.lang.Specification

class UserMapperSpec extends Specification {

    def "Test convert dto to entity"() {
        given:
        def dto = EnhancedRandom.random(UserDto.class)
        dto.id = 1
        when:
        def entity = UserMapper.INSTANCE.userDtoToEntity(dto)

        then:
        entity.id == dto.id
        entity.email == dto.email
        entity.name == dto.name
        entity.surname == dto.surname
        entity.username == dto.username
        entity.role.id == dto.role.id
        entity.organization.id == dto.organization.id
    }

    def "Test convert entity to dto"() {
        given:
        def entity = EnhancedRandom.random(UserEntity.class)
        entity.id = 1
        when:
        def dto = UserMapper.INSTANCE.userEntityToDto(entity)

        then:
        dto.id == entity.id
        dto.email == entity.email
        dto.name == entity.name
        dto.surname == entity.surname
        dto.username == entity.username
        dto.role.id == entity.role.id
        dto.organization.id == entity.organization.id
    }

    def "Test create user request to entity"() {
        given:
        def request = EnhancedRandom.random(CreateUserRequest.class)
        when:
        def entity = UserMapper.INSTANCE.createUserRequestToEntity(request)

        then:
        entity.email == request.email
        entity.name == request.name
        entity.surname == request.surname
        entity.username == request.username
    }

}
