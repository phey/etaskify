package com.etaskify.mapper

import com.etaskify.dao.TaskEntity
import com.etaskify.dto.TaskDto
import io.github.benas.randombeans.api.EnhancedRandom
import spock.lang.Specification

class TaskMapperSpec extends Specification {

    def "Test convert dto to entity"() {
        given:
        def dto = EnhancedRandom.random(TaskDto.class)
        dto.id = 1
        dto.completed = false
        when:
        def entity = TaskMapper.INSTANCE.taskDtoToEntity(dto)

        then:
        entity.id == dto.id
        entity.title == dto.title
        entity.description == dto.description
        entity.deadline == dto.deadline
        entity.completed == dto.completed
        entity.users.get(0).id == dto.users.get(0).id
        entity.organization.id == dto.organization.id
    }

    def "Test convert entity to dto"() {
        given:
        def entity = EnhancedRandom.random(TaskEntity.class)
        entity.id = 1
        entity.completed = false
        when:
        def dto = TaskMapper.INSTANCE.taskEntityToDto(entity)

        then:
        dto.id == entity.id
        dto.title == entity.title
        dto.description == entity.description
        dto.deadline == entity.deadline
        dto.completed == entity.completed
        dto.organization.id == entity.organization.id
    }

}
