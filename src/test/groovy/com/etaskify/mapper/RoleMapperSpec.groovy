package com.etaskify.mapper

import com.etaskify.dao.RoleEntity
import com.etaskify.dto.RoleDto
import io.github.benas.randombeans.api.EnhancedRandom
import spock.lang.Specification

class RoleMapperSpec extends Specification {

    def "Test convert dto to entity"() {
        given:
        def dto = EnhancedRandom.random(RoleDto.class)
        dto.id = 1
        when:
        def entity = RoleMapper.INSTANCE.roleDtoToEntity(dto)

        then:
        entity.id == dto.id
        entity.name == dto.name
    }

    def "Test convert entity to dto"() {
        given:
        def entity = EnhancedRandom.random(RoleEntity.class)
        entity.id = 1
        when:
        def dto = RoleMapper.INSTANCE.roleEntityToDto(entity)

        then:
        dto.id == entity.id
        dto.name == entity.name

    }

}
