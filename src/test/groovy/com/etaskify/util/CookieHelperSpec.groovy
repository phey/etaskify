package com.etaskify.util

import io.github.benas.randombeans.api.EnhancedRandom
import spock.lang.Specification

import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse

class CookieHelperSpec extends Specification {

    private CookieHelper cookieHelper

    def setup() {
        cookieHelper = new CookieHelper()
    }

    def "addCookies"() {
        setup:
        String organizationId = EnhancedRandom.random(String)
        HttpServletResponse response = Mock()

        when:
        cookieHelper.addCookie("cookie_key", organizationId, 1, response)
        then:
        1 * response.addCookie(_ as Cookie)
    }
}
