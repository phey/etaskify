
<h1 align="center"> eTaskify </h1> <br>

## Table of Contents

- [Introduction](#introduction)
- [API](#api) 

## Introduction

Introduction

Task management
Application for managing tasks for an organization.

Features:
Most features require logging in
Not authorized users have access to registration organization and creating admin user for organization

Admin can (only this organization):
Create users
List all users
Delete users
Create tasks and assign tasks to any user
Switch task status as completed
Assign task to users
List all tasks
Delete tasks

User(team member) can (only this organization):
List all users
Switch task status as completed/in-progress
List all tasks


Database structure 

![etaskify ERD model](docs/erd.png)

Using Spring Boot, Spring Security, Docker, PostgreSQL, Gradle, Liquibase, Logging, Swagger
Covered only service and mapper layer


## API 
# eTaskify app contracts
Please check http://localhost:8080/swagger-ui.html

## Run application locally : 
#### Open shell terminal
- #### ./gradlew clean build <br>
- #### docker-compose build
- #### docker-compose up

| End-point | Method | Decription |
|---|---|---|
| /v1/auth/signup  | POST | Register new admin user for new organization | 
| /v1/auth/signin  | POST | Login with username and password |

Test user: <br>
username : admin <br>
password : adminPass

 








